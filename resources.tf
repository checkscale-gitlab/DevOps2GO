#This uses the default VPC.  It WILL NOT delete it on destroy.
resource "aws_default_vpc" "default" {

}

resource "aws_security_group" "allow_ssh" {
  name        = "DevOps2GO"
  description = "Allow ports for control node"
  vpc_id      = aws_default_vpc.default.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "DevOps2GO" {
  # Ubuntu Server 20.04 LTS (HVM), SSD Volume Type in eu-central-1, adapt this to your region
  ami                    = "ami-0502e817a62226e03"
  instance_type          = "t2.micro"
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  tags                   = { Name = "DevOps2GO" }

  root_block_device {
    volume_size = 20
  }

  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file(var.private_key_path)
  }

  # copy ansible playbook to the remote instance, in order to then install the remaining tools and utilities with Ansible
  provisioner "file" {
    source      = "ansible"
    destination = "~"
  }

  # install the first tools and dependencies with a shell script, that will then launch our ansible playbook that will install our software
  provisioner "remote-exec" {
    script = "install_with_bash.sh"
  }
}
