# Read me

## What is this?

This project is aimed at [__DevOps__](https://en.wikipedia.org/wiki/DevOps "if you don't know what DevOps is, start here") beginners, students, start-ups, engineers on the go or anyone in need of a quickly available, reproducible environment for the tasks listed below (after "Congrats!"). For ease of use, I'll be providing links to all concepts within this document. You could be test-deploying your own [__Infrastructure-as-code__](https://en.wikipedia.org/wiki/Infrastructure_as_code) or __building__ and __deploying__ your __automatically tested__ applications within minutes. All you'll need is a copy of [Terraform](https://www.terraform.io/downloads.html "Download Terraform for any platform here, it's free and open source") on any machine available to you and an [Amazon Web Services (AWS)](https://aws.amazon.com) account. __A free-tier account will do__. In simple terms, [Terraform](https://www.terraform.io/downloads.html "Download Terraform for any platform here, it's free and open source") will use the code in this project to create a virtual computer and install several software packages that __[DevOps](https://en.wikipedia.org/wiki/DevOps "if you don't know what DevOps is, start here") engineers, developers and testers__ can use for their projects. __That is the "toolchain"__. It is called a chain because all tools together build a chain from software conception over testing and building to final deployment.

Since [Terraform](https://www.terraform.io/downloads.html "Download Terraform for any platform here, it's free and open source") is a single binary with no dependencies, and AWS is online, __this project is portable__. You don't need to be admin on your local machine either. If you're a road warrior forced to use the customers' computers on-site, or a student having to switch machines all the time, __you could run this from a USB-stick__. Just put everything in one folder and you're good to go. 

Hence the name, __DevOps2GO__.

## How do you use it?

1. Download a copy of this project using the download button on this page and unpack it, or use [Git](https://git-scm.com/docs) if you know how to do so.
2. Download a copy of [Terraform](https://www.terraform.io/downloads.html "Download Terraform for any platform here, it's free and open source"). Put it in your path if you can -then it will run anywhere on your machine-, if you can't or don't know how, put it in the root folder of this code, with the rest of the files.
3. Create a text file called __secrets.auto.tfvars__ (no .txt at the end) in the same folder that looks like the template at the end of this Readme. Replace the placeholders with your own information. The two first lines are for Terraform to access your AWS account and create your virtual machine. Here's how to [create and download your AWS access key ID and secret access key](https://www.msp360.com/resources/blog/how-to-find-your-aws-access-key-id-and-secret-access-key/). Please use the IAM access keys, not the root ones. Here's a short explanation [why](https://isecpartners.github.io/aws/2015/02/23/do_not_use_your_root_account.html). The third and fourth lines reference the private key you will use to access your machine using [SSH](https://en.wikipedia.org/wiki/Secure_Shell "Secure Shell"). The easiest way to create one is to [use the AWS EC2 console](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair
), then download the .pem file to your PC and store it outside of the folder of this code. But if you prefer you can also [create and upload your own](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#how-to-generate-your-own-key-and-import-it-to-aws). For the path to the private key (see template below), windows users should be aware that they need to double the backslashes. So, if your key is under C:\users\me\desktop\my_key.pem, then what your entry should look like is

```HCL
private_key_path = "C:\\users\\me\\desktop\\my_key.pem"
```

4. From the command line, navigate to the project's folder and run the command 'terraform init'. Terraform will make sure it is ready to run and tell you so.
5. Then run 'terraform plan -out plan'. Terraform will gather the information from the files and tell you when it's done.
6. Last, run 'terraform apply plan'. Terraform will create your virtual computer and install the software on it. You can follow the output to see it happening live. At the very end, Terraform will tell you the public URL of your node.

__Congrats!__

You now have a virtual computer full of useful software. You can:

- __manage your code with [Git](https://git-scm.com/docs)__
- __provision with [Terraform](https://www.terraform.io/downloads.html "Download Terraform for any platform here, it's free and open source")__
- __configure with [Ansible](https://www.ansible.com/use-cases/configuration-management)__ (using SSH for *nix, but WinRM is included to configure Windows machines)
- __build Software with [Maven](http://maven.apache.org), [Gradle](https://gradle.org) and [CMake](https://cmake.org)__
- __create and manage containers with [Docker](https://docs.docker.com)__
- __create [Kubernetes](https://kubernetes.io) clusters on [AWS](https://aws.amazon.com) with [eksctl](https://github.com/weaveworks/eksctl), manage them with [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/) and deploy cluster-based containerised applications with [Helm](https://helm.sh)__
- __orchestrate [Continuous Integration](https://en.wikipedia.org/wiki/Continuous_integration "Continuous Integration")/[Delivery](https://en.wikipedia.org/wiki/Continuous_delivery "Continuous Delivery")/[Deployment](https://en.wikipedia.org/wiki/Continuous_deployment "Continuous Deployment") ([CI/CD](https://en.wikipedia.org/wiki/CI/CD "CI/CD")) with [Jenkins](https://jenkins.io/doc/) or [GitLab](https://gitlab.com/) with your own [GitLab Runner](https://docs.gitlab.com/runner/)__

From the get-go, you'll be set to develop, build, test and deploy using some of the most popular languages (the first 50 to 60% on the [TIOBE Index](https://www.tiobe.com/tiobe-index/)), __C, C++, C#, Python, Java and Visual Basic .NET.__ Of course, you're free to add more.

You can now connect to your virtual computer through _[SSH](https://en.wikipedia.org/wiki/Secure_Shell "Secure Shell") on port 22_ using the URL provided by Terraform at the end of the creation process and your private key. The login is __ubuntu__. If you are unsure how this works, connect to your [AWS EC2 console](https://console.aws.amazon.com/ec2/). Under "Running instances", you'll be able to right-click on your newly created instance and select "connect". This will give you several possibilities to connect to your virtual computer.

## Final technical notes:

- As mentioned further up, references to __AWS and SSH keys__ are stored in a file called __secrets.auto.tfvars__ (see example below), which you will need to create and place __in the same folder__ as this project. It is not part of this repository. _Guess why._
- Also, the end-of-line formatting of the bash script __install_with_bash.sh__ needs to be __in UNIX format (LF)__, and not Windows (CR LF) or Mac (CR), or the script's execution will fail. The file _.gitattributes_ is there to take care of that, so you should be fine if you manage your code with Git or don't make any changes.
- Once Terraform has successfully created your node, you will need to __connect to it__ through SSH and __configure the software__ you intend to use.

For example, should you wish to use your newly crafted node to create virtual networks and machines on AWS ([Infrastructure-as-code](https://en.wikipedia.org/wiki/Infrastructure_as_code), remember?), you will need to add the AWS credentials you want to use to your configuration with the following command:

```BASH/CMD
aws configure                   # interactive, will ask for aws keys, region and output
```

You can then test your configuration is working by issuing a command that connects to AWS, for example:

```BASH/CMD
aws ec2 describe-instances      # will output the running instances
```

Should you wish to use Jenkins to automate tests, builds and deployment, you will have to open 127.0.0.1:8080 in a graphical browser to finish installation and configuration. I recommend tunnelling that port through your SSH tunnel to your local machine, but you can also open the port in the security group of your freshly created EC2 instance. Then it will be _accessible to the whole wide world_. Your call.

Should you wish to run your own GitLab runner, you'll need to configure it using the information listed under "Set up a specific Runner manually" in the Settings -> CI/CD -> Runners section of your GitLab project. For this use the command:

```BASH
sudo gitlab-runner register
```

```Reading directions
    /\ Instructions above /\
--- --- --- --- --- --- --- ---
    \/   Template below   \/
```

The _secrets.auto.tfvars_ looks like this:

```HCL
aws_access_key = "my aws access key"

aws_secret_key = "my aws secret key"

key_name = "my key name"

private_key_path = "local path to private key.pem"
```
